<?php

namespace PAJERdesign\CaptchaBundle\Controller;

use Gregwar\Captcha\CaptchaBuilderInterface;
use Gregwar\Captcha\PhraseBuilderInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CaptchaController extends AbstractController
{

    /**
     * @param ParameterBagInterface $parameterBag
     * @param SessionInterface $session
     * @param CaptchaBuilderInterface $captchaBuilder
     * @param PhraseBuilderInterface $phraseBuilder
     * @param string $key
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function generateCaptchaAction(
        ParameterBagInterface $parameterBag,
        SessionInterface $session,
        CaptchaBuilderInterface $captchaBuilder,
        PhraseBuilderInterface $phraseBuilder,
        string $key
    )
    {
        $config = $parameterBag->get('pajerdesign_captcha');
        $persistedOptions = $session->get($key, array());
        $config = array_merge($config, $persistedOptions);
        $phrase = $phraseBuilder->build($config['length'], $config['charset']);

        $captchaBuilder->setPhrase($phrase)
            ->setDistortion($parameterBag->get('pajerdesign_captcha.distortion'))
            ->setMaxFrontLines($parameterBag->get('pajerdesign_captcha.max_front_lines'))
            ->setMaxBehindLines($parameterBag->get('pajerdesign_captcha.max_behind_lines'))
            ->setMaxAngle($parameterBag->get('pajerdesign_captcha.max_angle'))
            ->setMaxOffset($parameterBag->get('pajerdesign_captcha.max_offset'))
            ->setIgnoreAllEffects($parameterBag->get('pajerdesign_captcha.ignore_all_effects'))
            ->setInterpolation($parameterBag->get('pajerdesign_captcha.interpolation'))
            ->setBackgroundImages($parameterBag->get('pajerdesign_captcha.background_images'))
        ;

        if ($parameterBag->get('pajerdesign_captcha.text_color')) {
            if (count($parameterBag->get('pajerdesign_captcha.text_color')) !== 3) {
                throw new \RuntimeException('text_color should be an array of r, g and b');
            }

            $color = $parameterBag->get('pajerdesign_captcha.text_color');
            $captchaBuilder->setTextColor($color[0], $color[1], $color[2]);
        }

        if ($parameterBag->get('pajerdesign_captcha.background_color')) {
            if (count($parameterBag->get('pajerdesign_captcha.text_color')) !== 3) {
                throw new \RuntimeException('text_color should be an array of r, g and b');
            }

            $color = $parameterBag->get('pajerdesign_captcha.background_color');
            $captchaBuilder->setBackgroundColor($color[0], $color[1], $color[2]);
        }

        $captchaContent = $captchaBuilder->build(
            $parameterBag->get('pajerdesign_captcha.width'),
            $parameterBag->get('pajerdesign_captcha.height'),
            null,
            null
        )->getGd();

        $persistedOptions['phrase'] = $phrase;
        $session->set($key, $persistedOptions);

        $response = new Response($captchaContent);
        $response->headers->set('Content-type', 'image/jpeg');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Cache-Control', 'no-cache');

        return $response;

    }


}