<?php

namespace PAJERdesign\CaptchaBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('pajerdesign_captcha');

        if (\method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            // BC layer for symfony/config 4.1 and older
            $rootNode = $treeBuilder->root('pajerdesign_captcha');
        }

        $rootNode
            ->children()
                ->arrayNode('text_color')
                    ->prototype('scalar')
                    ->end()
                    ->end()
                ->arrayNode('background_color')
                    ->prototype('scalar')
                    ->end()
                    ->end()
                ->arrayNode('background_images')
                    ->prototype('scalar')
                    ->end()
                    ->end()
                ->scalarNode('distortion')
                    ->defaultTrue()
                    ->end()
                ->scalarNode('max_front_lines')
                    ->defaultNull()
                    ->end()
                ->scalarNode('max_behind_lines')
                    ->defaultNull()
                    ->end()
                ->scalarNode('max_angle')
                    ->defaultValue(8)
                    ->end()
                ->scalarNode('max_offset')
                    ->defaultValue(5)
                    ->end()
                ->scalarNode('interpolation')
                    ->defaultTrue()
                    ->end()
                ->scalarNode('ignore_all_effects')
                    ->defaultFalse()
                    ->end()
                ->scalarNode('text_length')
                    ->defaultValue(5)
                    ->end()
                ->scalarNode('width')
                    ->defaultValue(150)
                    ->end()
                ->scalarNode('height')
                    ->defaultValue(30)
                    ->end()
                ->scalarNode('quality')
                    ->defaultValue(90)
                    ->end()
                ->scalarNode('charset')
                    ->defaultValue('abcdefghijklmnopqrstuvwxyz1234567890')
                    ->end()
                ->scalarNode('bypass_string')
                    ->defaultNull()
                    ->end()
                ->scalarNode('web_path')
                    ->defaultValue('%kernel.root_dir%/../public')
                    ->end()
                ->scalarNode('image_folder')
                    ->defaultValue('captcha')
                    ->end()
            
        ;

        return $treeBuilder;
    }
}